import UIKit
import Alamofire

class AuthSessionManager {
    static let current = AuthSessionManager()
    let session = Session(interceptor: AuthInterceptor())
}

class AuthInterceptor: RequestInterceptor {
    // MARK: - Properties
    var validToken: String?
    var isRefreshing: Bool = false
    var retryLimit = 3

    // MARK: - Conforming methods
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {

        let request = urlRequest

        completion(.success(request))
        
    }

    func retry(_ request: Request, for session: Session, dueTo error: Error,
               completion: @escaping (RetryResult) -> Void) {
        guard request.retryCount < retryLimit else {
            completion(.doNotRetry)
            return
        }
        print("\nretried; retry count: \(request.retryCount)\n")
        guard let statusCode = request.response?.statusCode else {
            completion(.doNotRetry)
            return
        }
        
        switch statusCode {
        case 200...299:
            completion(.doNotRetry)
        case 401:
            completion(.doNotRetry)
        case 403:
            completion(.doNotRetry)
        default:
            completion(.doNotRetry)
        }
    }

}
