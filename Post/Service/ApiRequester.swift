import Foundation
import Alamofire
import SwiftyJSON

class ApiRequest {
    
    static let shared = ApiRequest()
    
    private var sessionToken: String?
    private var expiredTimer: Timer?
    
    func request(_ endpoint: String, method: HTTPMethod, parameters: Parameters? = nil, encoding: JSONEncoding? = .default, headers: HTTPHeaders?, nav: UINavigationController, completion: @escaping (_ responseJson: AFDataResponse<Any>) -> Void) {
        self.trackingParam(endpoint: endpoint, headers: headers, parameters: parameters)
        AppHelper.shared.showLoader()
        AuthSessionManager.current.session.request(endpoint, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers, requestModifier: { $0.timeoutInterval = 60 }).validate(statusCode: 200..<300).responseJSON { response in
            AppHelper.shared.hideLoader()
            self.trackingResponse(response: nil, headers: response.response?.headers, statusCode: response.response?.statusCode)
            if (response.response?.statusCode == 401) {
                print("got invalid token, must refresh")
            }else {
                completion(response)
            }
        }
    }
    
    private func trackingParam(endpoint: String?, headers: HTTPHeaders?, parameters: Parameters?){
        print("api request endpoint ", endpoint as Any)
        print("api request header", headers as Any)
        print("api request param", parameters as Any)
    }
    
    private func trackingResponse(response: JSON?, headers: HTTPHeaders?, statusCode: Int?){
        print("api response headers", headers as Any)
        print("api response data", response as Any)
        print("api response status code", statusCode as Any)
    }
}

//MARK: - Mapping Failure Service
struct MappingError: Codable {
    var errorCode = ""
    var sourceSystem = ""
    var engMessage = ""
    var idnMessage = ""
    init (_ json : [String : Any]){
        print("cek json rsp error : \(json)")
        if let _errorCode = json["errorCode"] as? String{
            errorCode = _errorCode
        }
        if let _sourceSystem = json["sourceSystem"] as? String{
            sourceSystem = _sourceSystem
        }
        if let _engMessage = json["engMessage"] as? String{
            engMessage = _engMessage
        }
        if let _idnMessage = json["idnMessage"] as? String{
            idnMessage = _idnMessage
        }
    }
    
    var errorMessage : String {
        let language = NSLocale.current.languageCode!
        return language == "id" ? idnMessage : engMessage
    }
    
}

