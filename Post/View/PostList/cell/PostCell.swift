import UIKit

class PostCell: UITableViewCell {
    @IBOutlet weak var inputTitle: UITextField!
    @IBOutlet weak var inputContent: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func mapData(data: PostModel){
        inputTitle.text = data.title
        inputContent.text = data.content
        inputTitle.isEnabled = false
        inputContent.isEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
