import UIKit
import Alamofire
import SwiftyJSON

class PostListVC: UIViewController {
    @IBOutlet weak var tableview: UITableView!
    
    var dataPost: [PostModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        tableview.registerCellType(PostCell.self)
        tableview.rowHeight = 90
        tableview.reloadData()
        
        //api
        getDataPost()
    }
    
    func getDataPost(){
        print(":get data post")
        guard let nav = self.navigationController else {
            print(":get data post not exected")
            return
        }
        print(":get data post exected")
        let header = AppHelper.shared.prepareHeader()
        let url = "https://limitless-forest-49003.herokuapp.com/posts"
        AppHelper.shared.showLoader()
        ApiRequest.shared.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header, nav: nav) {
            response in
            AppHelper.shared.hideLoader()
            switch response.result {
            case .success:
                if let json = response.data {
                    self.dataPost = JSON(json).arrayValue.map { PostModel($0) }
                    self.tableview.reloadData()
                } else {
                    AlertHelper.shared.showAlert(self, title: "Alert", message: "Internal Server Error")
                }
            case .failure(_):
                AlertHelper.shared.showAlert(self, title: "Alert", message: "Internal Server Error")
            }
        }
    }

}

extension PostListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(PostCell.self, for: indexPath)
        let rowData = dataPost[indexPath.row]
        cell.mapData(data: rowData)
        return cell
    }
}
