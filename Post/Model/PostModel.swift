import Foundation
import SwiftyJSON

// MARK: - PostModel
class PostModel {
    var id: Int
    var title, content, publishedAt, createdAt: String
    var updatedAt: String

    init(id: Int, title: String, content: String, publishedAt: String, createdAt: String, updatedAt: String) {
        self.id = id
        self.title = title
        self.content = content
        self.publishedAt = publishedAt
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
    
    init (_ json: JSON) {
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.content = json["content"].stringValue
        self.publishedAt = json["publishedAt"].stringValue
        self.createdAt = json["createdAt"].stringValue
        self.updatedAt = json["updatedAt"].stringValue
    }
}
