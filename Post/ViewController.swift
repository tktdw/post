import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            print("move to post list")
            let vc = PostListVC()
            self.navigationController?.pushViewController(vc, animated: false)
        }
      
    }


}

