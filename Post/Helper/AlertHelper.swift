import UIKit

class AlertHelper {
    static let shared = AlertHelper()
    fileprivate init() {}
    
    func showAlert(_ viewController: UIViewController?,title: String, message: String, btnTitle: String,completionBlock :(() -> Void)? = nil){
        self.showAlertWithViewController(viewController, title: title, message: message) {
            completionBlock?()
        }
    }

    func showAlert(_ viewController: UIViewController?,title: String, message: String,completionBlock :(() -> Void)? = nil){
        self.showAlertWithViewController(viewController, title: title, message: message) {
            completionBlock?()
        }
    }

    //complitionBlock : ((_ done: Bool) ->Void)? = nil

    private func showAlertWithViewController(_ viewController: UIViewController?, title: String, message: String,completionBlock :(() -> Void)? = nil){
        var toastShowingVC :UIViewController!

        if let vc = viewController{
            toastShowingVC = vc
        }else{
          //  toastShowingVC = AppDelegate.getAppDelegate().window?.rootViewController
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
            guard let handler = completionBlock else{
                alert.dismiss(animated: false, completion: nil)
                return
            }
            handler()
            alert.dismiss(animated: false, completion: nil)
        }

        alert.addAction(okayAction)
        alert.view.tintColor = .black
        toastShowingVC.present(alert, animated: true, completion: nil)
    }
    
    private func showAlertWithViewController(_ viewController: UIViewController?, title: String, message: String, btnTitle: String,completionBlock :(() -> Void)? = nil){
        var toastShowingVC :UIViewController!

        if let vc = viewController{
            toastShowingVC = vc
        }else{
          //  toastShowingVC = AppDelegate.getAppDelegate().window?.rootViewController
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: btnTitle, style: .cancel) { (action) in
            guard let handler = completionBlock else{
                alert.dismiss(animated: false, completion: nil)
                return
            }
            handler()
            alert.dismiss(animated: false, completion: nil)
        }

        alert.addAction(okayAction)
        alert.view.tintColor = .black
        toastShowingVC.present(alert, animated: true, completion: nil)
    }
}
