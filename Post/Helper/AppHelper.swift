import Foundation
import Alamofire
import SVProgressHUD

class AppHelper {
    static let shared = AppHelper()
    
    func prepareHeader() -> HTTPHeaders{
        let accept = "application/json"
        var header = Dictionary<String,String>()
        header.updateValue(accept, forKey: "Accept")
        header.updateValue(accept, forKey: "Content-Type")
        header.updateValue("1.0.0", forKey: "app_version")
        header.updateValue("iOS", forKey: "os")
        header.updateValue(UIDevice.current.systemVersion, forKey: "os_version")
        header.updateValue("iPhone", forKey: "phone_brand")
        header.updateValue(UIDevice.current.model, forKey: "phone_type")

        return HTTPHeaders(header)
    }
    
    //MARK: Showing Loading
    let loaderSize = CGSize(width: 120, height: 120)
    func showLoader()
    {
        if !self.isLoaderOnScreen {
            SVProgressHUD.setDefaultStyle(.custom)
            SVProgressHUD.setDefaultMaskType(.custom)
            SVProgressHUD.setForegroundColor(UIColor.black)
            SVProgressHUD.setRingThickness(5)
            SVProgressHUD.show()
        }
    }


    var isLoaderOnScreen: Bool
    {
        return SVProgressHUD.isVisible()
    }
    func showError(withStatus status: String)
    {
        SVProgressHUD.showError(withStatus: status)
    }
    func showSuccess(withStatus status: String)
    {
        SVProgressHUD.showSuccess(withStatus: status)
    }


    func showLoader(withStatus status: String)
    {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.custom)
        if self.isLoaderOnScreen{
            self.updateLoader(withStatus: status)
        }else{
            SVProgressHUD.setBackgroundLayerColor(UIColor.white.withAlphaComponent(0.9))
            SVProgressHUD.setBackgroundColor(UIColor.white.withAlphaComponent(0.9))
            SVProgressHUD.setForegroundColor(.black)
            SVProgressHUD.show(withStatus: status)
            SVProgressHUD.setMinimumSize(loaderSize)
        }

    }

    func updateLoader(withStatus status: String)
    {
        SVProgressHUD.setStatus(status)
        SVProgressHUD.setMinimumSize(loaderSize)
    }

    func hideLoader()
    {
        if SVProgressHUD.isVisible(){
            SVProgressHUD.dismiss()
        }
    }
    
}
